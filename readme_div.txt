Project: Confalerts

Technologies: 

		1. Angular  
		
			material, routing, routing authorization, api calls, file handling
		
		2. PHP (CI)
		
			base urls, files- create, delete, directory manipulation.

Functionality extended to Admin:
1. Create a new conference with basic details + option to postpone file upload for later.
	
	Descriptors for conference: title, categories[multiple], to/from/archive dates, country, state, city, zipcode,
	website, email, ticket, price, description.
	
	Files Supported: Current Limit 1: Call for paper and image.
	
2. Update/ Delete this conference:

	Note: Previous sub categories are deleted each time an update occur
	A table for file uploads is shown to delete previous files, 
	Selection of new files automatically replace previous ones!

3. [ manually archive the conferences ]- runs a job and archives all the conferences due to their date.
	[Run a Cron Job to do the same]
	
4. Manage Submissions

	View the papers/ abstracts submitted by user and appropriately take action ->
	
	3.1 Approve
	
	3.2 Reject
	
	[Accordingly emails will be generated and sent to the user.]
	
5. [Generate PDF from Conf Details]
	
	
Functionality extended to User:

1. View different conferences

	[filter them based on search key terms and sort them according to price.]
	[ "pagination pending" ]
	
2. Submit abstract for the conferences they seem fit.

3. [Login to send paper]
