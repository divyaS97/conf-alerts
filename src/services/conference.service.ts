import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable} from 'rxjs';
import { Conference,Submission } from './conference';


@Injectable()
export class ConferenceService{
	
	conf_for_userviewing:any;
	conf_for_pendingsave:any;
	sub:any;
	
	constructor(private http: Http){

		
	}
	
	
	
	setCurrentConf(obj)
	{
		console.log("inside service conf, setting obj for viewwing");
		this.conf_for_userviewing=obj;
		
	}
	
	getCurrentConf()
	{
		console.log("inside service conf, getting obj for viewing");
		return this.conf_for_userviewing;
	}
	
	setForSave(obj)
	{
		console.log("inside service conf, setting obj for saving");
		this.conf_for_pendingsave=obj;
	}
	
	getForSave()
	{
		console.log("inside service conf, getting obj for saving");
		return this.conf_for_pendingsave;
	}
	
	
}