import { Injectable } from '@angular/core';
import { Http , Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Conference,Submission } from './conference';
import { LoginService } from './login.service';
import { ApiRouting } from './routes';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
 export class ConfigService{
	 public options:any;
	 public API_BASE_URL:any;
	 
	 private SetupArray=['Countries','States','Cities','Categories'];
	 
	 constructor (private http:Http, private loginSvc: LoginService){
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		this.options = new RequestOptions({ headers: headers });
		 
	 }
	 /*------------FETCHING DATA FROM API........--------------*/
	 
	getFormSetup(action="")
	{	
		let data= JSON.stringify(this.loginSvc.postcredentials());
		
		 if(this.SetupArray.includes(action)){
			return this.http.post(ApiRouting.API_BASE_URL+ 'api/' +`get${action}`,data,this.options).map((res)=>res.json(),(err)=>err);
		}
		else{
			console.log("Illegal REquest, config.service.ts");
		}
		
	}
	
	
	getConfById(id){
		
		let data=JSON.stringify(this.loginSvc.postcredentials());
		return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.getConfByIndex +id,data,this.options)
						.map((res)=>res.json(),(err)=>err);
						
	}
	
		
	SearchConf(obj){
		
		let data=JSON.stringify(this.loginSvc.postcredentials());
		return this.http.post('http://localhost/confalerts/api/index.php/fetchme/myconf',	
						data,this.options)
						.map((res)=>res.json(),(err)=>err);
						
	}
	
	getConfForEditById(id){
		
		let data=JSON.stringify(this.loginSvc.postcredentials());
		return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.getByIdForEdit +id,data,this.options)
						.map((res)=>res.json(),(err)=>err);
						
	}
	
	getUserById(id){
		
		let data=JSON.stringify(this.loginSvc.postcredentials());
		return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.getUserById +id,data,this.options)
						.map((res)=>res.json(),(err)=>err);
		
	}

	
	getConferencesFromApi(){
		
		let data=JSON.stringify(this.loginSvc.postcredentials());
		return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.getConferences,data,this.options)
						.map((res)=>res.json(),(err)=>err);
						
	}

	getSubmissionsFromApi(){
		//return this.http.get("../assets/submissions.json").map((res)=>res.json(),(err)=>err);
	
		let data=JSON.stringify(this.loginSvc.postcredentials());
		return this.http.post(ApiRouting.API_BASE_URL+ApiRouting.getSubmissions,data,this.options)
						.map((res)=>res.json(),(err)=>err);
	}
	
	
	
		 /*------------ SAVE TO DB REQUESTS TO API........--------------*/
	saveConference(data_incoming,action=""){
		
		let data= {...data_incoming,...this.loginSvc.postcredentials()};
		
		if(action=='short'){
			data= JSON.stringify(data);
			return this.http.post(ApiRouting.API_BASE_URL + ApiRouting.saveConference,data,this.options).map((res)=>res.json(),(err)=>err);
		}
		
	}
	
	saveCategory(data_incoming){
		
		 let data= JSON.stringify({...data_incoming,...this.loginSvc.postcredentials()});
		 return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.saveCategory,data,this.options).map((res)=>res.json(),(err)=>err);
	 }
	 
	saveRegistration(data_incoming){
		
		 let data= JSON.stringify({...data_incoming,...this.loginSvc.postcredentials()});
		 return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.saveRegistration,data,this.options).map((res)=>res.json(),(err)=>err);
	 }
	 
	 	/*------------UPDATE REQUESTS TO API........--------------*/
	updateConference(){
		// let obj= this.loginSvc.postcredentials();
		//data= {...data,...obj};
	}
	
	 changeFileStatus(){
		//  let obj= this.loginSvc.postcredentials();
		//data= {...data,...obj};
	 }
	 
	 /*-------------DELETE REQUESTS TO API.......---------------*/
	 deleteConf(data_incoming){
				 console.log(data_incoming);
				 let pair={id:data_incoming.id};	
				 
				 console.log({...pair,...this.loginSvc.postcredentials()});
				 
		let data= JSON.stringify({...pair,...this.loginSvc.postcredentials()});
		 return this.http.post(ApiRouting.API_BASE_URL+ApiRouting.deleteConf,data,this.options).map((res)=>res.json(),(err)=>err);
	 }
	 
	 deleteCategories(data_incoming){
		let data= {...data_incoming,...this.loginSvc.postcredentials()};
		return this.http.post(ApiRouting.API_BASE_URL+ApiRouting.deleteCategories,JSON.stringify(data),this.options).map((res)=>res.json(),(err)=>err);
	 }
	 
	 archive(){
		 console.log('hello world');
	 }
 }