export class ApiRouting {



    public static API_BASE_URL                  = ' http://localhost/confalerts/api/index.php/';
	
	/*LOGIN */
	 public static iamhome				='iam/home';
	 public static iamgoing 			='iam/going';
	  
 //Conf Handling
	 public static getConfByIndex 		= "api/getConfByIndex/";
	 public static getByIdForEdit 		= "api/getByIdForEdit/";
	 public static getUserById 	  		= "api/getUserById/";
	 public static getConferences 		= "api/getConferences";
	 public static getSubmissions 		= "api/getSubmissions";
	 public static saveConference 		= "api/saveConference";
	 public static saveCategory			= "api/saveCategory";
	 public static saveRegistration 	= "api/saveRegistration";
	 public static deleteConf			= "api/deleteConf";
	 public static deleteCategories 	= "api/deleteCategories";
		 
	//File Handling
	 public static	put					= "file/put";
	 public static  fetch				= "file/fetch/";
	 public static  deletefile			= "file/delete/"
	
}