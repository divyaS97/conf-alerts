export interface Conference{
	
		id: string;
		title: string;
		parentcat:string;
		subcat:string;
		fromdate:string;
		todate: string;
		archivedate: string;
		country: string;
		state: string;
		city: string;
		zipcode: string;
		website: string;
		email: string;
		ticketprice: string;
		description: string;
		
}

export interface Submission{
	
			file: string;
			type: string;
			conference: string;
			statusfile: string;
			userid: string;
}