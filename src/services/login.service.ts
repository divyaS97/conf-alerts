import { Inject, Injectable } from '@angular/core';
import { Http , Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { isStorageAvailable } from 'angular-webstorage-service';
import { ApiRouting } from './routes';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
 export class LoginService{
	 public options:any;
	 public API_BASE_URL:any;
	 public data:any;
	 
	// private subject = new Subject<any>();
	 isLoginSubject= new BehaviorSubject<boolean>(this.isLoggedIn());
	 User :any;
	 
	 constructor (private http:Http,
				  private router:Router,
				  @Inject(LOCAL_STORAGE)
				  private storage: WebStorageService ){
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		this.options = new RequestOptions({ headers: headers });		
		console.log(JSON.parse(this.storage.get('currentUser')));
	 }
	 	 
	 /*------------FETCHING DATA FROM API........--------------*/
	login(data){
		return this.http.post(ApiRouting.API_BASE_URL+ApiRouting.iamhome ,data,this.options).map((res)=> res.json() ,(err)=>err);
	}

	logout(){
		let data=this.getCurrentUser();
		return this.http.post(ApiRouting.API_BASE_URL+ApiRouting.iamgoing ,data,this.options).map((res)=>res.json(),(err)=>err);
	}
	
	setCurrentUser(data){
	   this.storage.set('currentUser',JSON.stringify(data));	
	   this.isLoginSubject.next(true);
    
	}
	
	getCurrentUser(){
		return  JSON.parse(this.storage.get('currentUser'));
	}
	
	removeCurrentUser(){
		this.storage.remove('currentUser');
		this.isLoginSubject.next(false);
		return true;
	}
	
	getLoginStatus(): Observable<boolean>{
		return this.isLoginSubject.asObservable();
	}
	
	isLoggedIn(): boolean {
			return !!this.storage.get('currentUser');
	}
	
	postcredentials(){
		
		let obj=JSON.parse(this.storage.get('currentUser'));
		
		  var obj2:any = {}
			obj2.userid = obj.value.id;
			obj2.token= obj.value.access_key;
		
		return obj2;
	}
 }