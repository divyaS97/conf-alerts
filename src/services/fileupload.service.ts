import { Injectable } from '@angular/core';
import { Http , Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Conference,Submission } from './conference';
import { LoginService } from './login.service';
import { ApiRouting } from './routes';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
 export class FileUploadService{
	 public options:any;
	 public API_BASE_URL:any;
	 
	 constructor (private http:Http, private loginSvc: LoginService){
	
		let headers = new Headers(/* { 'Content-Type': 'multipart/form-data' } */);
		this.options = new RequestOptions({ headers: headers });
		
	 }
	 /*------------FETCHING DATA FROM API........--------------*/
	

	
	SaveFile(formdata){
		 return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.put,formdata,this.options).map((res)=>res.json(),(err)=>err);
	 }

	 FetchFile(formdata){
		 
		 let data=JSON.stringify(this.loginSvc.postcredentials());
		 return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.fetch +formdata,data,this.options).map((res)=>res.json(),(err)=>err);
	 }
	 
	 DeleteFile(formdata){
		 return this.http.post(ApiRouting.API_BASE_URL+ ApiRouting.deletefile +formdata,this.options).map((res)=>res.json(),(err)=>err);
	 }
 }