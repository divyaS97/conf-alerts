import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditconfComponent } from './editconf.component';

describe('EditconfComponent', () => {
  let component: EditconfComponent;
  let fixture: ComponentFixture<EditconfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditconfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditconfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
