import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,NgForm, FormControl} from '@angular/forms';
import { LoginService} from '../../services/login.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	loginForm:FormGroup;
	msg;
	tried:boolean= false;
	
	loggedIn: Observable<boolean>;
	
  constructor(private loginSvc: LoginService,
			  private formBuilder:FormBuilder,
			  private router:Router) { 
			  
	this.loginForm=formBuilder.group({
		Email:new FormControl(),
		Password:new FormControl()
	});	  	  
			  }

  ngOnInit() {
	  
	
		this.loggedIn = this.loginSvc.getLoginStatus();	
		
		this.loggedIn.subscribe((res)=> {
			
			if(res==true)
			{
				this.router.navigateByUrl('/');
			}
		});
  }
  
  PostData(loginForm:NgForm)
  {
	  let data=this.loginForm.value;
	this.tried=true;
	  this.loginSvc.login(data).subscribe(
	  (jsondata)=>
	  {   
		 
		if(jsondata.type=="true")
		{	
			this.loginSvc.setCurrentUser(jsondata);
			this.router.navigateByUrl(`/`);
		}
		else
		{
			this.msg=jsondata.msg;
		}
	  },
	(err)=> {if(err.status==0)
		{
			
			this.msg=" Seems like XAMPP is refusing to connect";
		}
	
	} );
  
  }
} //class close
