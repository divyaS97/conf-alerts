import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, NgForm} from '@angular/forms';
import { Injectable } from '@angular/core';
import { ConfigService } from '../../../services/config.service';
import { ConferenceService } from '../../../services/conference.service';
import { FileUploadService } from '../../../services/fileupload.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

	@Input() id;   // Id is coming from parent-> participate and being stored as id
	newRegForm:FormGroup;
	
	result={"type":null, "msg":""};
	fileid=null;
	load=false;
	
  constructor( private formbuilder:FormBuilder,
  private configSvc:ConfigService,
  private conSvc:ConferenceService,
  private fileUploadSvc:FileUploadService) { 
  
  this.newRegForm= formbuilder.group({
	Organization:new FormControl(),
	Fullname:new FormControl(),
	Email:new FormControl(),
	Mobnumber:new FormControl(),
	Title:new FormControl() 
	  
  });
  }

  ngOnInit() {
  }
  
  PostData(newRegForm:NgForm){
	  let data=this.newRegForm.value;
	  
	  let temp={
				Password:"null",
				Type:"abstract",
				EventId:this.id,
				file:this.fileid
			}
	  
	  data={...data,...temp};
	  
	  console.log(data);
	    this.configSvc.saveRegistration(data).subscribe((jsondata)=>{
		 this.result=jsondata; 
	  },(err)=>{
		    console.log(`{err} oops! error occured`);
	  });
  }
  
  upload(files:File[]){
	  
	  let fileArr= Array.from(files);
	  this.postToServer(0,fileArr);
  }
  
  postToServer(fileindex, fileArr){
	  
	  this.load=true;
	  var formData= new FormData();
	  if(fileArr[fileindex])
	  {
		  formData.append('file',fileArr[fileindex]);
		  formData.append('filecategory','abstract');
		  
		  this.fileUploadSvc.SaveFile(formData).subscribe(
		  (jsondata)=>{
			  if(jsondata.type=='true')
			  {
				  this.fileid=jsondata.data.id;
			  }
			  else
			  {
				  console.log("Oops, declined!!");
			  }
		  },
		  (err)=>{
			  console.log("Oops, declined!!");
		  }
		  );
		  
	  }
	    else
	  {
			  console.log('no file for upload');
	  }
		  
	  this.load=false;
	  
  }

}
