import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConferenceService } from '../../services/conference.service';
import { ConfigService } from '../../services/config.service';

import { Location } from '@angular/common';

@Component({
  selector: 'app-participate',
  templateUrl: './participate.component.html',
  styleUrls: ['./participate.component.css']
})
export class ParticipateComponent implements OnInit {	
	conf_details_evil:any = {};
	conf_details_good:any;
	
	id:any;
	opendate="20-7-2018";
	closedate="3-8-2018";
	
	showdetails=false;
	showabstract=false;
	toggleview="Show Details";
	
	filterkeys=['description','to_date', 'from_date','country','title','id', 'callforpaper','picture','category' ];
  constructor(private actRouter: ActivatedRoute,
				private conSvc:ConferenceService,
				private configSvc:ConfigService,
				private location:Location) {

}

 ngOnInit() {
	
		this.conf_details_evil.title="";
		this.id=this.actRouter.snapshot.paramMap.get('id');
		
		this.configSvc.getConfById(this.id).subscribe(
		(data)=>{ 
			
			this.conf_details_evil=data.data[0];
			console.log(this.conf_details_evil);
			this.conf_details_good = Object.assign({}, this.conf_details_evil);
			this.filterkeys.forEach(e => delete this.conf_details_good[e]);
		},
		(err)=>{ console.log(err); }
		);
	 
    /* }); */
		
	/* this.conf_details_evil=this.conSvc.getCurrentConf();
	this.conf_details_good = Object.assign({}, this.conf_details_evil);
	this.filterkeys.forEach(e => delete this.conf_details_good[e]); */
}


toggle(str,target){
	
	if(str=='details')
	{	
		this.showdetails= !this.showdetails;
		this.toggleview= (this.toggleview=='Show Details')? "Hide Details":"Show Details";
	}
	else if(str=='s_abstract')
	{
	
		if(this.showabstract==false)
		{
			setTimeout(function() {
					  window.scroll({
					  top: 1000,
					  behavior: "smooth"
					  });	
					},1)	
			
		}
		this.showabstract= !this.showabstract;
	}
	


}

//toggle button ID, change show hide of register and details
compareFn = (a, b) => {
	
}; //avoids sorting of object

	goback(){
		this.location.back();
	}

	
	
}
