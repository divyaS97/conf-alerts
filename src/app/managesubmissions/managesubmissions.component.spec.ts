import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagesubmissionsComponent } from './managesubmissions.component';

describe('ManagesubmissionsComponent', () => {
  let component: ManagesubmissionsComponent;
  let fixture: ComponentFixture<ManagesubmissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagesubmissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagesubmissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
