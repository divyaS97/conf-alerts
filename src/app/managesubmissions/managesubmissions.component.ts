import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ConferenceService } from '../../services/conference.service';
import { ConfigService } from '../../services/config.service';
import { FormControl, FormGroup, FormBuilder, NgForm, ReactiveFormsModule} from '@angular/forms';
import { Submission } from '../../services/conference';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import * as rxjs from 'rxjs';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'app-managesubmissions',
  templateUrl: './managesubmissions.component.html',
  styleUrls: ['./managesubmissions.component.css']
})
export class ManagesubmissionsComponent implements OnInit, OnDestroy {

	fstatus: FormControl= new FormControl();
	submissionsarray:any ;
	message;
	filteredsubs:any;
	fstatusSubsciption:any;
	
  constructor( private conSvc: ConferenceService, 
				private configSvc: ConfigService,
				public dialog: MatDialog) { }
				
	 openDialog(id): void {
		 	 
		 
		 
		const dialogRef = this.dialog.open(UserProfileDialog, {
				  width: '500px',
				  data: {id:id}
				});
	 
		dialogRef.afterClosed().subscribe(result => {
		  console.log('The dialog was closed');
		});
  }
  
  ngOnInit() {
	  
	this.configSvc.getSubmissionsFromApi().subscribe((resJson)=>{
	this.submissionsarray=resJson.data;
	this.filteredsubs=this.submissionsarray;
	
	console.log(this.submissionsarray);
	  }, (err)=>{
		  this.message="There are no submissions at the moment, please try again later";
	  });
  
	this.fstatusSubsciption= this.fstatus.valueChanges
	/* .debounceTime(400) */
	/* .distinctUntilChanged() */
	.subscribe(
	fstatus=> {
		let filterBy= fstatus? fstatus:null;
		let filteredsubs= filterBy
		? this.submissionsarray.filter(item=> item.statusfile.indexOf(filterBy) !== -1)
		: this.submissionsarray;
		
		this.filteredsubs= filteredsubs;
		
	})
  
  }
  
  ask(str){
	  
	  if(str=="Approve")
	  {
		  if(confirm('This will send a confirmation Email to User, Are you sure about this?'))
		  {
			  console.log("Calling required Service");
		  }

	  }
	  else if(str=="Reject")
	  {
		  if(!confirm('This will send a rejection Email to User, Are you sure about this?'))
		  {
				console.log("Calling required Service");
		  }
	  }
}

 ngOnDestroy(){
	 this.fstatusSubsciption.unsubscribe();
 }
}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'userprofile.html',
})
export class UserProfileDialog {

	user:any;
	
  constructor(
    public dialogRef: MatDialogRef<UserProfileDialog>,
	private configSvc: ConfigService,
    @Inject(MAT_DIALOG_DATA) public data) {
		
		this.configSvc.getUserById(data.id).subscribe((resJson)=>{
		 if(resJson.type=="true")
		 {
			this.user=resJson.data[0];	
			console.log(this.user);
		 } else console.log('err');
		 
		 },
		 (err)=>{console.log(err);});
		
	}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

