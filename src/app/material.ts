import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule,MatDialogModule],
  exports: [MatButtonModule, MatCheckboxModule,MatDialogModule],
})
export class MaterialModule { }