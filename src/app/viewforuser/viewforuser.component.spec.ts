import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewforuserComponent } from './viewforuser.component';

describe('ViewforuserComponent', () => {
  let component: ViewforuserComponent;
  let fixture: ComponentFixture<ViewforuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewforuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewforuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
