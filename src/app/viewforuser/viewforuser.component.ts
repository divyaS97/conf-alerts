import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, NgForm, ReactiveFormsModule} from '@angular/forms';
import { ConferenceService } from '../../services/conference.service';
import { ConfigService } from '../../services/config.service';

import {MatTooltipModule} from '@angular/material/tooltip';
import * as rxjs from 'rxjs';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'app-viewforuser',
  templateUrl: './viewforuser.component.html',
  styleUrls: ['./viewforuser.component.css']
})

export class ViewforuserComponent implements OnInit, OnDestroy{


  country: FormControl = new FormControl();
  conferences:any;
  filteredconferences:any;
  message:any;
  count=1;
  countrySubscription:any;
  
  searched:any;

    constructor(private router: Router,private conSvc:ConferenceService
	,private configSvc:ConfigService) { }

  
  ngOnInit() {
	  this.configSvc.getConferencesFromApi().subscribe((jsondata)=>{
		  this.conferences=jsondata.data;
		  
		  this.filteredconferences=this.conferences;
	  },(err)=>{
		  this.message="Sorry! Data Base was lost on the way, we are working on this!";
	  });
	
	 this.countrySubscription= this.country.valueChanges
	.debounceTime(400)
	.distinctUntilChanged()
	.subscribe(
	country=> {
		let filterBy = country? country :null;
		
		let filteredconferences= filterBy
		? this.conferences.filter(item => item.country.indexOf(filterBy) !== -1)
		:this.conferences;
		
		this.filteredconferences= filteredconferences;
	})
   
  }
  
  clicked(confObj,action){
		if(action=="deleteconference")
		{
			this.configSvc.deleteConf(confObj).subscribe(
			(data)=>{
				
				alert(data.msg);
				window.location.reload();
			},
			(err)=>{ console.log(err);}
			
			);
		}
		else /* if(action=="participate") */
		{
			this.router.navigateByUrl(`/${action}/${confObj.id}`);
			  // this.router.navigate(['/participate', confObj.id]);
		}/* 
		else {
			 this.conSvc.setCurrentConf(confObj);
			this.router.navigateByUrl(`/${action}`);
		} */
  }
  
  ngOnDestroy(){
	  
	  this.countrySubscription.unsubscribe();
  }
  
  search(str){
	  
		let obj={key:str};
	  	this.configSvc.SearchConf(obj).subscribe(
			(data)=>{	
				this.searched=data.data;
			},
			(err)=>{ console.log(err);}
			
			);
  }
  
  
  archive(){
		
			this.configSvc.archive().subscribe(
		(data)=>{ 
				if(data.type=='true')
				{
					alert('successful');
				}
				else{
					console.log('err'); 
				}
		
		},
		(err)=>{ console.log(err); }
		);
		
	}
	
}
