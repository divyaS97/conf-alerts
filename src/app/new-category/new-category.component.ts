import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { ConfigService} from '../../services/config.service';
import { Subscription } from 'rxjs/subscription';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.css']
})
export class NewCategoryComponent implements OnInit {

	newCatForm:FormGroup;
	categories:any;
	categories_child:any;
	categories_par:any;
	result={"type":null, "msg":""};
	childCatSubscription:Subscription;
	Parentcat:any;
	
  constructor(private formbuilder:FormBuilder, private configSvc:ConfigService) { 
	
	this.newCatForm= formbuilder.group({
		Parentcat: new FormControl(),
		Newcat: new FormControl()	
	}); 
  }

  ngOnInit() {
	  this.configSvc.getFormSetup("Categories").subscribe(
	(jsondata)=>{ this.categories=jsondata.data;
				this.categories_par= this.categories.filter(t => t.parent_id === "0");
	},(err)=>{ console.log(err); });
	
  
	this.childCatSubscription= this.newCatForm.get('Parentcat').valueChanges
	.subscribe(
	(data)=>{
		this.categories_child=this.categories.filter(t => t.parent_id === data);
	}
	);
  
  
  }
  
  PostData(newCatForm:NgForm)
  {
	  let data= this.newCatForm.value;
	  console.log(data);
	  this.configSvc.saveCategory(data).subscribe((jsondata)=>{
			 this.result=jsondata; 
	  },
	  (err)=>{
				this.result.msg="BACKEND CALL FAILED, PLEASE TRY AGAIN";
		      console.log(`{err} oops! error occured`);
	  }
	  );
	  
	    newCatForm.reset();
  }
	  
  

}
