import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-viewcat',
  templateUrl: './viewcat.component.html',
  styleUrls: ['./viewcat.component.css']
})
export class ViewcatComponent implements OnInit {

 
	catObj: any ={
	"IT":["AI", "ML", "Blockchain"],
	"Agriculture":"Soil Testing"
};
	count:number =1;
	del:boolean=false;
		
	catForDelete:any;
	deleteArray: any[]=[];
	

  constructor(private configSvc: ConfigService) { }

  ngOnInit() {
	this.configSvc.getFormSetup("Categories").subscribe(
	(jsondata)=>{ 
					this.catObj= this.manipulate(jsondata.data); 
					this.catForDelete= jsondata.data;
				
				},
	(err)=>{ console.log("oops something went wrong");}
	)
  
  }

manipulate(OBJ) {
	
	let keyname,keyvalue,tmp,search_id;
		let data={};
	let objtemp=OBJ;
	OBJ.forEach(Obj=>{
		
			if(Obj.parent_id==0){
							
				let pair={};
				keyname=Obj.category_name;
				search_id= Obj.id;
				tmp=objtemp.filter(t => t.parent_id === search_id);
				keyvalue=tmp.map( tmp=> tmp.category_name);
				pair[keyname]=keyvalue;
				data={...data,...pair};
			}
		});
	return data;
}

	pushintoarray(num){
		
		let objIndex = this.catForDelete.findIndex((obj => obj.id == num));
		
		if(this.deleteArray.includes(num))
			{
				this.catForDelete[objIndex].delforselected=false;
				this.deleteArray.splice(this.deleteArray.indexOf(num), 1)
			
			}
			else {
				this.catForDelete[objIndex].delforselected=true;
				this.deleteArray.push(num);
			
			}

	}
	
	objready()
	{
		console.log(this.deleteArray);
		let ListToDelete= {listtodel:this.deleteArray};
	
	this.configSvc.deleteCategories(ListToDelete).subscribe(
	(jsondata)=>{ 
					if(jsondata.type=='TRUE')
					{
						alert(' Categories successfully deleted');
						window.location.reload();
					}
					else if(jsondata.type=='FALSE')
					{
						alert(' OOps, something went wrong');
					}
				},
	(err)=>{ console.log("oops something went wrong");}
	)
	}

}
