import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-view-conf',
  templateUrl: './view-conf.component.html',
  styleUrls: ['./view-conf.component.css']
})

/* export catobj={
"IT":"AI",
	"Agriculture":"Soil Testing"
} */

export class ViewConfComponent implements OnInit {

	catObj: any ={
	"IT":["AI", "ML", "Blockchain"],
	"Agriculture":"Soil Testing"
}

  constructor(private configSvc: ConfigService) { }

  ngOnInit() {
	console.log("hello worwld");
	this.configSvc.getFormSetup("Categories").subscribe(
	(jsondata)=>{ console.log(jsondata);
this.catObj= this.manipulate(jsondata); },
	(err)=>{ console.log("oops something went wrong");}
	)
  
  }

manipulate(OBJ) {
	
	let data,pair,keyname,keyvalue,tmp,search_id;	
	
	OBJ.forEach(Obj=>{
		
			if(Obj.parent_id==0){
				
				keyname=Obj.category_name;
				search_id= Obj.id;
				tmp=OBJ.filter(t => t.id === search_id);
				keyvalue=tmp.map( tmp=> tmp.category_name);		
			}
		
		pair={keyname:keyvalue};
		data={...data,...pair};
		});

	console.log(data);
	return data;
}

}