import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AppComponent } from './app.component';
import { CreateConfComponent } from './create-conf/create-conf.component';
import { ViewConfComponent } from './view-conf/view-conf.component';
import { NewCategoryComponent } from './new-category/new-category.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ViewforuserComponent } from './viewforuser/viewforuser.component';
import { ParticipateComponent } from './participate/participate.component';
import { RegisterComponent } from './participate/register/register.component';
import { UsersuccessComponent } from './usersuccess/usersuccess.component';
import { ManagesubmissionsComponent,UserProfileDialog } from './managesubmissions/managesubmissions.component';
import { ConfNextComponent, PopUpImage } from './create-conf/conf-next/conf-next.component';
import { StorageServiceModule } from 'angular-webstorage-service';
import { LoginComponent } from './login/login.component';
import { ViewcatComponent } from './viewcat/viewcat.component';
import { EditconfComponent } from './editconf/editconf.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';


/***service***/
import { ConfigService } from '../services/config.service';
import { ConferenceService } from '../services/conference.service';
import { LoginService } from '../services/login.service';
import { FileUploadService } from '../services/fileupload.service';
import { AuthGuardService } from './auth/auth.service';


const routes: Routes = [
	
	{path:'newcategory', 
	 component: NewCategoryComponent, 
	 canActivate: [AuthGuardService]},
	
	{path:'viewforuser'   , 
		component:ViewforuserComponent, 
		canActivate: [AuthGuardService]},
		
	{path:'participate/:id'   , 
		component:ParticipateComponent,
		canActivate: [AuthGuardService]},
		
	{path:'usersuccess', 
		component:UsersuccessComponent,
		canActivate: [AuthGuardService]},
		
	{path:'viewcategory' , 
		component:ViewcatComponent,
		canActivate: [AuthGuardService]},
		
	{path:'conffileupload', 
		component:ConfNextComponent,
		canActivate: [AuthGuardService]},
		
	{path:'newconference' , 
		component:CreateConfComponent,
		canActivate: [AuthGuardService]} ,
		
	{path:'editconference/:id' , 
		component:CreateConfComponent,
		canActivate: [AuthGuardService]} ,
	
	
	{path:'viewconference', 
		component:ViewConfComponent,
		canActivate: [AuthGuardService]}  ,
		
	{path:'managesubmissions', 
		component:ManagesubmissionsComponent,
		canActivate: [AuthGuardService]},
		
	{path:'login',component:LoginComponent},
	
	{path: '**', redirectTo: ''}
	];


@NgModule({
  exports: [

  ],
  declarations: [
    AppComponent,
    CreateConfComponent,
    ViewConfComponent,
    NewCategoryComponent,
    SidebarComponent,
    ViewforuserComponent,
    ParticipateComponent,
    RegisterComponent,
    UsersuccessComponent,
    ManagesubmissionsComponent,
    ConfNextComponent,
    LoginComponent,
    ViewcatComponent,
    EditconfComponent,
	UserProfileDialog,
	 PopUpImage
  ],
  imports: [
    BrowserModule,
	ReactiveFormsModule,
	HttpModule,
	HttpClientModule,
	StorageServiceModule,
	BrowserAnimationsModule,
	MaterialModule,
	RouterModule.forRoot(routes) 
  ],
  providers: [
  ConfigService,
  ConferenceService,
  LoginService,
  AuthGuardService,
  FileUploadService
  ],
  bootstrap: [AppComponent],
  entryComponents: [UserProfileDialog,
  PopUpImage
  ]
  
})
export class AppModule { }
