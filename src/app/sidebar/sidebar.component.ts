import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

export const menuAdmin: any[]=[
	{path:'/newconference', title:'New conference'},
	{path:'/newcategory', title:'New Category'},
	/* {path:'/viewconference', title:'View conference'}, */
	{path:'/viewcategory', title:'View/Del Category'},
	{path:'/viewforuser', title:'View For User'},
	{path:'/usersuccess', title:'User Success'},
	{path:'/managesubmissions', title:'Manage Submissions'},
	{path:'/login', title:'Login'}

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})


export class SidebarComponent implements OnDestroy {
	
	loggedIn: Observable<boolean>;

	public menuItems: any[];
  constructor( private loginSvc:LoginService, private router:Router) {
		  this.menuItems=menuAdmin.filter(i => i);
		  this.loggedIn = this.loginSvc.getLoginStatus();

  }

  ngOnInit() { }

	ngOnDestroy(){
		this.loginSvc.removeCurrentUser();
	}
	
	logout(){
			console.log("logout");
			this.loginSvc.logout().subscribe
			(
				(jsondata)=>
				{ console.log(jsondata);
					 
					if(jsondata.outcome=="true")
					{ //removed from backend
						this.loginSvc.removeCurrentUser();	//removed from angular
						this.router.navigateByUrl('/login');
					}	
						else {
							console.log("cant logout");
						} 
					
				},
				(err)=> { console.log(err);}
			); 
		}
}
