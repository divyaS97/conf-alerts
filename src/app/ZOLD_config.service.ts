import { Injectable } from '@angular/core';
import { Http , Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
 export class ConfigService{
	 public options:any;
	 public API_BASE_URL:any;
	 constructor (private http:Http){
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		this.options = new RequestOptions({ headers: headers });
		this.API_BASE_URL='http://localhost/confalerts/api/savetodb.php?action_type=';

		 
	 }
	 
	 saveConference(data){
		 return this.http.post(this.API_BASE_URL+'saveConference',data,this.options).map((res)=>res.json(),(err)=>err);
	 }
 }