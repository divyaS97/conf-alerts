import { Component } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
//import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'confalerts';
  
  
  constructor(private loginSvc:LoginService, private router:Router){
 	  
	   this.loginSvc.getLoginStatus().subscribe
		  (
			stats => {
						 if(!stats)
						 {
							router.navigateByUrl('/login'); 
						 }
					  }
		  ); 
  }
}
