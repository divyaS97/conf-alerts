import { Component, OnInit,OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, NgForm} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from '../../services/config.service';
import { ConferenceService } from '../../services/conference.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as rxjs from 'rxjs';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


@Component({
  selector: 'app-create-conf',
  templateUrl: './create-conf.component.html',
  styleUrls: ['./create-conf.component.css']
})


export class CreateConfComponent implements OnInit{

	id=null;		//foredit
	currentConf:any; //for edit
	
	Catsubscription:any;		//filter category
	
	subcategories:any;
	parentcategories:any;
	newconfForm:FormGroup;
	countries:any;
	states:any;
	cities:any;
	result={"type":null, "msg":""};
 	categories:any;
	showtime:boolean;
	
  constructor( private formbuilder:FormBuilder,private configSvc:ConfigService, private conSvc: ConferenceService,
				private actRouter: ActivatedRoute,private router:Router) 
	{
		this.newconfForm= formbuilder.group({	
				Title:new FormControl(),
				Parentcat:new FormControl(),
				Subcat:new FormControl(),
				Fromdate:new FormControl(),
				Todate:new FormControl(),
				Archivedate:new FormControl(),
				Country:new FormControl(),
				State:new FormControl(),
				City:new FormControl(),
				Zipcode:new FormControl(),
				Website:new FormControl(),
				Email:new FormControl(),
				Ticketprice:new FormControl(),
				Description:new FormControl()
			});
  }

	ngOnInit() 
	{
	
		if(this.router.url.includes('/editconference'))
		{
			this.id=this.actRouter.snapshot.paramMap.get('id');
			
			this.configSvc.getConfForEditById(this.id).subscribe(
			(data)=>
			{
				this.currentConf=data.data[0];
				console.log(this.currentConf);
				if(this.currentConf=== undefined) 	alert("Data was lost on the way, please go back and try again");  
				else{
					this.newconfForm.setValue({
					Title		:this.currentConf.title,
					Parentcat	:null,
					Subcat 		:this.currentConf.category.map(cat=>{	return cat.def_cat_id	}),
					Fromdate	:this.currentConf.from_date,
					Todate		:this.currentConf.to_date,
					Archivedate :this.currentConf.archive_date,
					Country		:this.currentConf.country,
					State		:this.currentConf.state,
					City		:this.currentConf.city,
					Zipcode		:this.currentConf.zipcode,
					Website		:this.currentConf.website,
					Email		:this.currentConf.email,
					Ticketprice :this.currentConf.tprice,
					Description :this.currentConf.description,	
					})
					}
			},(err)=>{console.log(err);}
			);
			
		} //if edit	
	
	
		//D IS JSONDATA
	this.configSvc.getFormSetup("Countries").subscribe((D) => { this.countries=D.data; },(err)=>{ console.log(err); });
  
	this.configSvc.getFormSetup("States").subscribe((D) => { this.states=D.data; },(err)=>{console.log(err);});
  
	this.configSvc.getFormSetup("Cities").subscribe((D)=> {this.cities=D.data;},(err)=>{console.log(err);});
	
	this.configSvc.getFormSetup("Categories").subscribe(
	(D)=>{ this.categories=D.data;
			this.parentcategories=this.categories.filter(t => t.parent_id === '0' );  
			this.subcategories=D.data.filter(t => t.parent_id !== '0' ) },
	(err)=>{console.log(err);});
	
	}
	
	update(val){ this.subcategories=this.categories.filter( t=> t.parent_id === val);}
	

  PostData(newconfForm:NgForm){
	  
	  let data=this.newconfForm.value;
	  if(this.id!=null)
	  {  
		let pair={id:this.currentConf.id};
		  data={...data,...pair};
	  }
	  
	  this.configSvc.saveConference(data,"short").subscribe(
	  (jsondata)=>{  this.result=jsondata; 
					this.showtime=true; 
						console.log(this.result.type); },
					(err)=>
					{ console.log(err); this.showtime=true;  }
					);
	  
	//  newconfForm.reset();
  }
  

  Navigate(newconfForm:NgForm){  
	  let data=this.newconfForm.value;
	  
	  if(this.id!=null)
	  { 
	  let fileObj={'callforpaper':this.currentConf.callforpaper,'picture':this.currentConf.picture};
	  let pair={id:this.currentConf.id};
	  data={...data, ...fileObj,...pair}
	  }
	  
	  this.conSvc.setForSave(data);
	  this.router.navigateByUrl(`/conffileupload`);  
	}
  
	
 }


//Object.assign(data,)  
	  //let pair={action_type:"saveconference"};
	 // data={...data,...pair};
	  /*  this.httpClient.post('http://localhost/confalerts/php/savetodb.php', data)
	  .subscribe((data)=>{ console.log(data)}, (error)=>{ console.log(error)}) 
	  {form_data:'Amit'}
	  */

