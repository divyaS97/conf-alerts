import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ConfigService} 	 from '../../../services/config.service';
import { ConferenceService } from '../../../services/conference.service';
import { FileUploadService } from '../../../services/fileupload.service';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-conf-next',
  templateUrl: './conf-next.component.html',
  styleUrls: ['./conf-next.component.css']
})

export class ConfNextComponent implements OnInit {
		showtime:boolean;
	confdetails:any;
	fileid:any;
	load:boolean=false;
	fileids: any = {
		'callforpaper':'',
		'picture':''
	};
	edit:boolean=false;
	files:any;
	result={"type":null, "msg":""};

  constructor( 
  private configSvc:ConfigService,
  private conSvc:ConferenceService,
  private fileUploadSvc:FileUploadService,
  public dialog: MatDialog  ) { 
  
  }

   popup(files): void {
    const dialogRef = this.dialog.open(PopUpImage, {
      width: '250px',
      data: {file: files}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
   
    });
  }
  
  
  ngOnInit() {  
	 this.confdetails=this.conSvc.getForSave();
	 if(this.confdetails.id!=null ||this.confdetails.id!=undefined)
	 {
		 this.edit=true;
		 this.fileUploadSvc.FetchFile(`${this.confdetails.callforpaper}/${this.confdetails.picture}`).subscribe(
		 (result)=>{
			 this.files=result.data;
			 if(result.type==false){
				 this.files=null;
			 }
			 
			 console.log(this.files);
		 },
		 (err)=>{
			 console.log('err');
		 }
		 );
		 
	 }
	 console.log(this.confdetails);
  }
  
  PostData(){
	  
	  let data={...this.confdetails,...this.fileids};
	  this.configSvc.saveConference(data,"short").subscribe(
	  (jsondata)=>{
		this.result=jsondata; 
					this.showtime=true; 
	  }, 
	  (err)=>{console.log(`{err} oops! error occured`);}
	  );
  }

  upload(files: File[], type){ 
	
	  let fileArr= Array.from(files);
	  this.postToServer(0,fileArr,type);
  }
  
  postToServer(fileindex,fileArr,type)
  {
	  this.load=true;   
	  var formData= new FormData();
	  if(fileArr[fileindex])
	  {
			formData.append('file',fileArr[fileindex]);
			
			// file category is abstract/ call for paper/ image;
			formData.append('filecategory',type);
			
			//APPENDING ID FOR EDIT CASE
			if(this.confdetails.id != null || this.confdetails.id != undefined )
			{
				formData.append('fileid',this.confdetails.id);
			}
			
			
			this.fileUploadSvc.SaveFile(formData).subscribe(
			  (jsondata)=> { 
								console.log(jsondata);
								this.fileids[type]=jsondata.data.id;
							},
			  (err)=> { console.log(err) }); 
	  }
	  else
	  {
			  console.log('no file for upload');
	  }
		  
	  this.load=false;
  }
  
  deletethis(id){
	  
	 this.fileUploadSvc.DeleteFile(id).subscribe(
		 (result)=>{
			 if(result==false){
				 alert("can't delete");
			 }
			 else window.location.reload();
			 console.log(this.files);
		 },
		 (err)=>{
			 console.log('err');
		 }
		 );
	 
  }
  
}


@Component({
  selector: 'dialog-overview-example-dialog',
  template: '<img src="{{data.file}}" alt="conf photo" height="150" width="150"/>'
})
export class PopUpImage {

  constructor(
    public dialogRef: MatDialogRef<PopUpImage>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

