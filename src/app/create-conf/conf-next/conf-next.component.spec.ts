import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfNextComponent } from './conf-next.component';

describe('ConfNextComponent', () => {
  let component: ConfNextComponent;
  let fixture: ComponentFixture<ConfNextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfNextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfNextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
