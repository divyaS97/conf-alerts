import { Injectable } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Subscription, Observable } from 'rxjs';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate{
	
	//subscription:Subscription;
	loggedIn: Observable<boolean>;
	action:boolean;
	
	constructor(public loginSvc: LoginService,
				public router:Router)
	{}
	
	public async isAuthenticated(){
				  
			/*  	  let val:any;
				  this.loggedIn = this.loginSvc.getLoginStatus();
					
			 this.loggedIn.map((res)=> {
			
			if(res==true)
			{
				this.action= true;
			}
			else this.action= false;
		});  */
 
	}
	
	canActivate() {
		
		/* let action= this.isAuthenticated();
		console.log(`$ action is ${action}`);
		 if(action){
			return true; 
		 } 
		 else {
			 console.log("inside false");
			 this.router.navigateByUrl('/login');
			 return false;
		 } */
		 	  return this.loginSvc.getLoginStatus();
					
			/*  this.loggedIn.subs((res)=> {
			
			if(res==true)
			{
				return true;
			}
			else return false;
		}); */
	}
}